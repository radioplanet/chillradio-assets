The work in this repository is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International Public License.
The license can be found [here](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). For a summary please see [this](https://creativecommons.org/licenses/by-nc-nd/4.0/) page.

If you want to adapt or use my work for commercial puproses, please send me a private message on twitter ([@charliebrittdev](https://twitter.com/charliebrittdev)).
